# Trade processor

Processes trade groups from an input XML file, accumulates their values and outputs a CSV with the result (accepted if the accumulated value from all trades in the group are at or below the limit, rejected otherwise, and pending if the group is incomplete and is missing some trades).

# Running it

    python3 bnp-test.py fixtures/input.xml
    diff results.csv fixtures/resultExample.csv

# Assumptions

* This was developed on Python 3.7.3 and there is no guarantee it will run on earlier versions.
* The XML file conforms to the supplied specification. There's very little error handling around malformed input files.
* The XML file is considered safe and from a trusted source. No effort has been made to protect against maliciously-crafted input files. The XML parser used *may* be vulnerable to certain attacks.
* In any trade within a group, both the `NumberOfTrades` and `Limit` parameters `must` remain consistent. For efficiency purposes the accumulator relies on values from the currently processing trade instead of the stored value.
* The number of trades within a group **must not** exceed the `NumberOfTrades`. The accumulator generates a result immediately upon reaching the expected number of trades. Future trades for that group are currently ignored, but this behavior should not be relied upon.
* No effort has been made to define which line separator to use in output files (whether LF or CRLF). They will differ between operating systems as Python defaults to the OS's conventions for files opened in text mode.

# Extra features

* Duplicate Trade IDs within a group will be ignored.
* Output file * log file paths can be set as optional command-line arguments. See `bnp-test.py --help` for details.

# Testing

    python3 tests.py
