import argparse
import logging

from server import main

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process trades.')
    parser.add_argument('input', type=argparse.FileType())
    parser.add_argument('--output', type=argparse.FileType('w'), default='results.csv')
    parser.add_argument('--log-file', type=str, default='server.log')
    args = parser.parse_args()

    logging.basicConfig(filename=args.log_file, level=logging.DEBUG)

    main(args.input, args.output)
