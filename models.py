import itertools
import logging
from collections import Counter, defaultdict, namedtuple
from enum import Enum

Trade = namedtuple('Trade', ['correlation_id', 'number_of_trades', 'limit', 'trade_id', 'value'])


class Status(Enum):
    ACCEPTED = 'Accepted'
    PENDING = 'Pending'
    REJECTED = 'Rejected'


Result = namedtuple('Result', ['correlation_id', 'number_of_trades', 'status'])


class Accumulator:
    def __init__(self):
        self.accumulated_values = Counter()  # correlation_id: accumulated value so far
        self.trade_ids = defaultdict(list)  # correlation_id: list of trade IDs seen so far for this group
        self.seen = Counter()  # correlation_id: how many trades we've seen for this group
        self.numbers_of_trades = {}  # correlation_id: number of trades (since we need to output it in the result)
        self.results = {}  # correlation_id: trade status (accepted or rejected; pending's will be collected at the end)

    def process_trade(self, trade):
        if trade.correlation_id in self.results:
            # If we already generated a result for this trade group, we skip.
            logging.warning('Already generated result for trade %s', trade)
            return

        if trade.trade_id in self.trade_ids[trade.correlation_id]:
            # Duplicate trade, ignore it
            logging.warning('Duplicate trade ID for trade %s', trade)
            return

        self.numbers_of_trades[trade.correlation_id] = trade.number_of_trades

        if trade.number_of_trades == 1:
            # Short-circuit for unique trades, don't waste resources and generate a result immediately
            result = Status.ACCEPTED if trade.value <= trade.limit else Status.REJECTED
            logging.info('Generating result %s for unique trade %s', result, trade)
            self.results[trade.correlation_id] = result
        else:
            seen = self.seen[trade.correlation_id]
            accumulated = self.accumulated_values[trade.correlation_id]

            if trade.number_of_trades == seen + 1:
                # Let's generate a result now
                result = Status.ACCEPTED if accumulated + trade.value <= trade.limit else Status.REJECTED
                logging.info('Reached expected number of trades for %s, generating result %s', trade, result)
                self.results[trade.correlation_id] = result

                # Don't keep unneeded values around in memory
                del self.seen[trade.correlation_id]
                del self.accumulated_values[trade.correlation_id]
                del self.trade_ids[trade.correlation_id]  # no longer needed as we'd reject based on correlation_id
            else:
                logging.info('Remembering trade %s. Expecting more trades for this group.', trade)
                self.seen[trade.correlation_id] = seen + 1
                self.accumulated_values[trade.correlation_id] = accumulated + trade.value
                self.trade_ids[trade.correlation_id].append(trade.trade_id)

    def collate_results(self):
        sorted_ids = sorted(itertools.chain(self.results.keys(), self.seen.keys()))

        for correlation_id in sorted_ids:
            if correlation_id in self.results:
                yield Result(
                    correlation_id=correlation_id,
                    number_of_trades=self.numbers_of_trades.pop(correlation_id),
                    status=self.results.pop(correlation_id)
                )
            else:
                number_of_trades = self.numbers_of_trades.pop(correlation_id)

                logging.warning(
                    'Only seen %s out of %s trades for group with correlation ID %s, generating pending result',
                    self.seen[correlation_id],
                    number_of_trades,
                    correlation_id
                )
                yield Result(
                    correlation_id=correlation_id,
                    number_of_trades=number_of_trades,
                    status=Status.PENDING
                )
