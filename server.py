import csv
import logging
from xml.etree import ElementTree

from models import Accumulator, Trade


def main(input_file, output_file):
    logging.info('Starting server with input file %s, output file %s', input_file.name, output_file.name)

    xml = ElementTree.parse(input_file)
    results_writer = csv.writer(output_file)

    trade_elements = xml.getroot()
    accumulator = Accumulator()

    for trade_element in trade_elements:
        logging.debug('Parsing trade %s', ElementTree.tostring(trade_element))

        try:
            trade = Trade(
                correlation_id=trade_element.attrib['CorrelationId'],
                number_of_trades=int(trade_element.attrib['NumberOfTrades']),
                limit=int(trade_element.attrib['Limit']),
                trade_id=trade_element.attrib['TradeID'],
                value=int(trade_element.text)
            )
        except (KeyError, ValueError):
            logging.exception('Skipping malformed trade element')
        else:
            accumulator.process_trade(trade)

    logging.info('Collecting & writing results')
    results = accumulator.collate_results()

    results_writer.writerow(['CorrelationID', 'NumberOfTrades', 'State'])

    for trade_result in results:
        results_writer.writerow([trade_result.correlation_id, trade_result.number_of_trades, trade_result.status.value])
