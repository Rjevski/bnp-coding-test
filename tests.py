import unittest

from models import Accumulator, Result, Status, Trade


class AccumulatorTestCase(unittest.TestCase):
    def setUp(self):
        self.accumulator = Accumulator()

    def test_result_short_circuit(self):
        """If we have a result make sure we don't bother processing anything else"""
        trade = Trade(
            correlation_id=1,
            number_of_trades=2,  # the accumulator should normally expect a second trade
            limit=100,
            trade_id=1,
            value=10
        )
        self.accumulator.results[trade.correlation_id] = True  # value doesn't matter, we just need to set the key
        self.accumulator.process_trade(trade)

        self.assertNotIn(trade.correlation_id, self.accumulator.seen)
        self.assertNotIn(trade.correlation_id, self.accumulator.trade_ids)
        self.assertNotIn(trade.correlation_id, self.accumulator.accumulated_values)

    def test_trade_id_short_circuit(self):
        """If we have seen this Trade ID before make sure we don't bother processing anything else"""
        trade = Trade(
            correlation_id=1,
            number_of_trades=2,  # the accumulator should normally expect a second trade
            limit=100,
            trade_id=1,
            value=10
        )
        self.accumulator.trade_ids[trade.correlation_id] = [trade.trade_id]
        self.accumulator.process_trade(trade)

        self.assertNotIn(trade.correlation_id, self.accumulator.seen)
        self.assertNotIn(trade.correlation_id, self.accumulator.accumulated_values)

    def test_single_trade_short_circuit(self):
        """If it's a single trade group, make sure we don't bother accumulating and instead create a result"""
        trade = Trade(
            correlation_id=1,
            number_of_trades=1,
            limit=100,
            trade_id=1,
            value=10
        )

        # Make the accumulator believe it's seen this trade already. The short-circuit would ignore this
        # but otherwise this will give us an incorrect result we can assert against
        # as the accumulator will still return a good value even without the short-circuit.
        self.accumulator.seen[trade.correlation_id] = 2
        self.accumulator.process_trade(trade)

        self.assertNotIn(trade.correlation_id, self.accumulator.accumulated_values)
        self.assertEqual(self.accumulator.results[trade.correlation_id], Status.ACCEPTED)

    def test_accumulator_remembers_trades(self):
        """Make sure we increment the 'seen' counter when we process multi-trade groups."""
        trade = Trade(
            correlation_id=1,
            number_of_trades=3,
            limit=100,
            trade_id=1,
            value=10
        )
        self.accumulator.process_trade(trade)

        self.assertEqual(self.accumulator.seen[trade.correlation_id], 1)

        trade = Trade(
            correlation_id=1,
            number_of_trades=3,
            limit=100,
            trade_id=2,
            value=10
        )

        self.accumulator.process_trade(trade)

        self.assertEqual(self.accumulator.seen[trade.correlation_id], 2)

    def test_accumulator_accumulates_values(self):
        """Make sure we accumulate values when we process multi-trade groups."""
        trade = Trade(
            correlation_id=1,
            number_of_trades=3,
            limit=100,
            trade_id=1,
            value=10
        )
        self.accumulator.process_trade(trade)

        self.assertEqual(self.accumulator.accumulated_values[trade.correlation_id], trade.value)

        trade = Trade(
            correlation_id=1,
            number_of_trades=3,
            limit=100,
            trade_id=2,
            value=-10
        )

        self.accumulator.process_trade(trade)

        self.assertEqual(self.accumulator.accumulated_values[trade.correlation_id], 0)

    def test_accepts_below_limit_single(self):
        """Make sure we accept single-trade groups if we're below the limit."""
        trade = Trade(
            correlation_id=1,
            number_of_trades=1,
            limit=100,
            trade_id=1,
            value=10
        )
        self.accumulator.process_trade(trade)

        results = list(self.accumulator.collate_results())

        self.assertListEqual(results, [
            Result(correlation_id=trade.correlation_id, number_of_trades=trade.number_of_trades, status=Status.ACCEPTED)
        ])

    def test_rejects_above_limit_single(self):
        """Make sure we reject single-trade groups if we're above the limit."""
        trade = Trade(
            correlation_id=1,
            number_of_trades=1,
            limit=100,
            trade_id=1,
            value=101
        )
        self.accumulator.process_trade(trade)

        results = list(self.accumulator.collate_results())

        self.assertListEqual(results, [
            Result(correlation_id=trade.correlation_id, number_of_trades=trade.number_of_trades, status=Status.REJECTED)
        ])

    def test_accepts_below_limit_multi(self):
        """Make sure we accept multi-trade groups if we're below the limit."""
        trade = Trade(
            correlation_id=1,
            number_of_trades=2,
            limit=100,
            trade_id=1,
            value=10
        )
        self.accumulator.process_trade(trade)

        trade = Trade(
            correlation_id=1,
            number_of_trades=2,
            limit=100,
            trade_id=2,
            value=10
        )
        self.accumulator.process_trade(trade)

        results = list(self.accumulator.collate_results())

        self.assertListEqual(results, [
            Result(correlation_id=trade.correlation_id, number_of_trades=trade.number_of_trades, status=Status.ACCEPTED)
        ])

    def test_rejects_above_limit_multi(self):
        """Make sure we reject multi-trade groups if we're above the limit."""
        trade = Trade(
            correlation_id=1,
            number_of_trades=2,
            limit=100,
            trade_id=1,
            value=10
        )
        self.accumulator.process_trade(trade)

        trade = Trade(
            correlation_id=1,
            number_of_trades=2,
            limit=100,
            trade_id=2,
            value=100
        )
        self.accumulator.process_trade(trade)

        results = list(self.accumulator.collate_results())

        self.assertListEqual(results, [
            Result(correlation_id=trade.correlation_id, number_of_trades=trade.number_of_trades, status=Status.REJECTED)
        ])

    def test_frees_up_resources_after_result_is_generated(self):
        """Make sure we delete seen, accumulated & Trade IDs after generating a result."""
        trade = Trade(
            correlation_id=1,
            number_of_trades=2,
            limit=100,
            trade_id=1,
            value=10
        )
        self.accumulator.process_trade(trade)

        # Now we should have some stuff in seen & accumulated since we're expecting another trade

        self.assertIn(trade.correlation_id, self.accumulator.seen)
        self.assertIn(trade.correlation_id, self.accumulator.accumulated_values)
        self.assertIn(trade.correlation_id, self.accumulator.trade_ids)

        trade = Trade(
            correlation_id=1,
            number_of_trades=2,
            limit=100,
            trade_id=2,
            value=100
        )
        self.accumulator.process_trade(trade)

        # At this point we've generated a result so we no longer need seen/accumulated_values/trade IDs

        self.assertNotIn(trade.correlation_id, self.accumulator.seen)
        self.assertNotIn(trade.correlation_id, self.accumulator.accumulated_values)
        self.assertNotIn(trade.correlation_id, self.accumulator.trade_ids)

    def test_pending_trade_generation(self):
        """Make sure we generate "pending" results for groups missing some trades."""
        trade = Trade(
            correlation_id=1,
            number_of_trades=2,
            limit=100,
            trade_id=1,
            value=10
        )
        self.accumulator.process_trade(trade)

        results = list(self.accumulator.collate_results())

        self.assertListEqual(results, [
            Result(correlation_id=trade.correlation_id, number_of_trades=trade.number_of_trades, status=Status.PENDING)
        ])

    def test_result_sorting(self):
        """Make sure we return results sorted by correlation ID."""
        trades = [
            Trade(
                correlation_id=1,
                number_of_trades=1,
                limit=100,
                trade_id=1,
                value=10
            ),
            Trade(
                correlation_id=18,
                number_of_trades=1,
                limit=100,
                trade_id=1,
                value=10
            ),
            Trade(
                correlation_id=14,
                number_of_trades=1,
                limit=100,
                trade_id=1,
                value=10
            ),
            Trade(
                correlation_id=5,
                number_of_trades=1,
                limit=100,
                trade_id=1,
                value=10
            ),
            Trade(
                correlation_id=101,
                number_of_trades=1,
                limit=100,
                trade_id=1,
                value=10
            ),
            Trade(
                correlation_id=2,
                number_of_trades=1,
                limit=100,
                trade_id=1,
                value=10
            ),
            Trade(
                correlation_id=50,
                number_of_trades=1,
                limit=100,
                trade_id=1,
                value=10
            )
        ]

        for trade in trades:
            self.accumulator.process_trade(trade)

        results = list(self.accumulator.collate_results())

        expected_results = [
            Result(correlation_id=1, number_of_trades=1, status=Status.ACCEPTED),
            Result(correlation_id=2, number_of_trades=1, status=Status.ACCEPTED),
            Result(correlation_id=5, number_of_trades=1, status=Status.ACCEPTED),
            Result(correlation_id=14, number_of_trades=1, status=Status.ACCEPTED),
            Result(correlation_id=18, number_of_trades=1, status=Status.ACCEPTED),
            Result(correlation_id=50, number_of_trades=1, status=Status.ACCEPTED),
            Result(correlation_id=101, number_of_trades=1, status=Status.ACCEPTED)
        ]

        self.assertListEqual(results, expected_results)


if __name__ == '__main__':
    unittest.main()
